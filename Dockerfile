FROM ubuntu

RUN apt-get update -y
RUN apt-get upgrade -y
RUN apt-get install wget -y
RUN wget https://github.com/prometheus/node_exporter/releases/download/v1.2.2/node_exporter-1.2.2.linux-amd64.tar.gz
RUN tar xvfz node_exporter-1.2.2.linux-amd64.tar.gz


ENTRYPOINT ["./node_exporter-1.2.2.linux-amd64/node_exporter"]
