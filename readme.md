# Node Exporter
> Prometheus's node exporter exposes hardware and kernel related metrics.  
> This repo will host a Dockerfile that builds an ubuntu image with node exporter installed and forwarding metrics.

## Overview
---
- The `Dockerfile` provided will use an ubuntu base image
- This repo serves to provide an example of the metrics output by `Prometheus Node Exporter`
- This can be used for mock metrics data


## Build
---
- To build the docker image run the following command from within the same directory as the `Dockerfile` in this repo
```
docker build -t <image_tag> .
# Example
docker build -t node_exporter_ubuntu .
```
> the tag name can be w.e you want, if you are pushing this up to a registry, make sure to follow docker guidelines and syntax

## Running
---
- Run a container with the image built above
```
docker run -t -d -p 9100:9100 <image_name>

# Example

docker run -t -d -p 9100:9100 node_exporter_ubuntu

# You can also expose different ports on the host or run multiple exporter instances

docker run -t -d 9101:9100 node_exporter_ubuntu
```

- Access the metrics
```
http://<hostname>:9100/metrics
or
http://localhost:9100/metrics
or
http://<container_ip>:9100/metrics
```


## References
---
- https://prometheus.io/docs/guides/node-exporter/#installing-and-running-the-node-exporter
- https://prometheus.io/download/#node_exporter (download)

